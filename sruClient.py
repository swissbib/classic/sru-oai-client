
import requests
import re



regex = re.compile("\n")
pFindIter = re.compile("<recordData>(<record.*?>.*?</record>)</recordData>", re.DOTALL | re.MULTILINE)
pCursor = re.compile("<nextCursormark>(.*?)</nextCursormark>", re.DOTALL | re.MULTILINE)

baseURL = 'http://sru.swissbib.ch/sru/search/defaultdb'



def formatCursorQuery(cursorToken):
    params = {
        'query': 'dc.possessingInstitution any B476 B550 B410 B569 B412 B501 B555 B404 B900 B901 B905 B906 B500 B571 B554 B507 B466 B470 E30 B452 B467 B490 B492 B465 B464 B505 B565 B525 B583 B531 B557 B552 B503 B575 B527 B529 B498 B415 B488 B591 B478 B400 B451 B542 B540 B534 B589 B599 B521 AND dc.format=BK02??00 AND dc.date <= 1950 AND dc.date >= 1700 NOT dc.localcode any erarabs erarabe Z01erara',
        'operation': 'searchRetrieve',
        'recordSchema': 'info:srw/schema/1/marcxml-v1.1-light',
        'maximumRecords': '100',
        'startRecord': '0',
        'recordPacking': 'XML',
        'availableDBs': 'defaultdb',
        'sortKeys': 'Submit query',
        'x-info-10-get-holdings' : 'true',
        'cursor': cursorToken
        }

    return params

def findCursorToken(content):
     match =  pCursor.search(content)
     if match:
         return match.group(1)
     else:
         raise RuntimeError("no cursor token found")


def fetchContent(cursor):
    #print(baseURL +  "?" + '&'.join('{}={}'.format(key, value) for key, value in formatCursorQuery(cursor).items()))
    result = requests.get(baseURL, params=formatCursorQuery(cursor))
    text = result.text
    text = regex.sub("", text)
    return text

def openOutputFile():
    return open("bern.xml","w")

def closeOutputFile(fileHandle):
    if not fileHandle is None:
        fileHandle.close()

def writeContent(fileHandle, text):
    fileHandle.write(text + "\n")

def searchSingleRecords(content):
    iterator = re.finditer(pFindIter, content)

    for singleRecord in iterator:
        yield singleRecord.group(1)
    
def writeSingleRecords(fileHandle, content):
    for singleRecord in searchSingleRecords(content):
        writeContent(fileHandle,singleRecord)



fileHandle = openOutputFile()

writeContent(fileHandle, "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>")
writeContent(fileHandle, "<collection>")

cursor = ''

content = fetchContent(cursor)
fetchedCursor = findCursorToken(content)
writeSingleRecords(fileHandle,content)

while (cursor != fetchedCursor ):
    cursor = fetchedCursor
    content = fetchContent(fetchedCursor)
    fetchedCursor = findCursorToken(content)
    writeSingleRecords(fileHandle,content)

writeContent(fileHandle, "</collection")
closeOutputFile(fileHandle)