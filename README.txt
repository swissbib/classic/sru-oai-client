A. Installation of virtual environment and install dependencies

1) create venv
cd [root directory of repository]
python3 -m venv ./venv/

2) activate just created virtual environment
source ./venv/bin/activate

now the venv should be used for all python activities
check it: which pip
should give something like this:
/home/swissbib/environment/code/swissbib.repositories/sru_oai_client/venv/bin/pip
(venv) swissbib@UB-18-PC-13:~/environment/code/swissbib.repositories/sru_oai_client$

3) install necessary requirements into virtual environment
pip install -r requirements.txt


B. use script
1) change into root directory of script
for Silvia: swissbib@sb-udb7:/swissbib_index/google/sru_oai_client

2) activate virtual environment

source ./venv/bin/activate
(compare checks in A2 to verify if environment is really activated

3) change script for your use case:
(done for the Basel example)
vi sruClient.py

line 17: change the query
line 47: change the output file

4) run the script (with the virtual environment)  (check again if it is active)

python3 sruClient.py &

wait until it's ready and change the parameters for the next run

of course this "hand done" steps are soon configurable...